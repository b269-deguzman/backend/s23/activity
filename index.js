	let trainer ={
		name: 'Justine De Guzman',
		age: 20,
		pokemon: ["Pikachu","Charizard","Bulbasaur"],
		friends: {
			hoenn: ['May','Max'],
			kanto: ['Brock', 'Misty']
		},
		talk: function(number){
			console.log(this.pokemon[number] + '! I choose you!');
		}

	}
	console.log(trainer);
	console.log('Result of dot notation: ');
	console.log(trainer.name);
	console.log('Result of square bracket notation: ');
	console.log(trainer['pokemon']);
	console.log('Result of talk method');
	console.log(trainer.talk(2));



	function Pokemon(name, level){
		this.name = name;
		this.level = level;
		this.health = Math.round(((this.level*this.level)/2));
		this.attack = this.level*2;

		this.tackle = function(target){
			let healthAfterDamage = target.health - this.attack;
				target.health = healthAfterDamage;
			console.log(this.name + " tackeled "+ target.name);
			if(target.health <=0){
				this.faint();
			}else{
			console.log(target.name + "'s health is now reduced to "+ healthAfterDamage );
			}
		}
		this.faint= function(){
			console.log(this.name + " fainted.")
		}
	}

	let charizard = new Pokemon("Charizard", 30);
	let pikachu = new Pokemon("Pikachu", 25);
	let bulbasaur = new Pokemon("Bulbasaur",40);

	console.log(charizard);
	console.log(pikachu);
	console.log(bulbasaur);

	
	bulbasaur.tackle(pikachu);

	bulbasaur.tackle(pikachu);
	bulbasaur.tackle(pikachu);

	charizard.tackle(pikachu);
		console.log(pikachu);
	charizard.tackle(pikachu);
	